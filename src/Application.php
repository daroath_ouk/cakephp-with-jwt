<?php
declare(strict_types=1);

namespace App;

use Cake\Core\Configure;
use Cake\Routing\Router;
use Cake\Http\BaseApplication;
use Cake\Http\MiddlewareQueue;
use Cake\Core\ContainerInterface;
use Cake\ORM\Locator\TableLocator;
use Cake\Datasource\FactoryLocator;
use Authorization\AuthorizationService;
use Authentication\AuthenticationService;
use Cake\Routing\Middleware\AssetMiddleware;
use Psr\Http\Message\ServerRequestInterface;
use Cake\Http\Middleware\BodyParserMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Authorization\AuthorizationServiceInterface;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Authentication\AuthenticationServiceInterface;
use Authentication\Identifier\IdentifierInterface;
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Authorization\Policy\OrmResolver;
use Authorization\Middleware\AuthorizationMiddleware;
use Authentication\Middleware\AuthenticationMiddleware;
use Authentication\AuthenticationServiceProviderInterface;

class Application extends BaseApplication
    implements AuthenticationServiceProviderInterface,
    \Authorization\AuthorizationServiceProviderInterface
{
    public function bootstrap(): void
    {
        // Call parent to load bootstrap from files.
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        } else {
            FactoryLocator::add(
                'Table',
                (new TableLocator())->allowFallbackClass(false)
            );
        }

        if (Configure::read('debug')) {
            $this->addPlugin('DebugKit');
        }

        // Load more plugins here
        $this->addPlugin('Authentication');
        $this->addPlugin('Authorization');
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware(MiddlewareQueue $middlewareQueue): MiddlewareQueue
    {
        $middlewareQueue
            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(Configure::read('Error')))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime'),
            ]))

            // Add routing middleware.
            // If you have a large number of routes connected, turning on routes
            // caching in production could improve performance. For that when
            // creating the middleware instance specify the cache config name by
            // using it's second constructor argument:
            // `new RoutingMiddleware($this, '_cake_routes_')`
            ->add(new RoutingMiddleware($this))

            // Parse various types of encoded request bodies so that they are
            // available as array through $request->getData()
            // https://book.cakephp.org/4/en/controllers/middleware.html#body-parser-middleware
            ->add(new BodyParserMiddleware())

            ->add(new AuthenticationMiddleware($this))
            ->add(new AuthorizationMiddleware($this));

            // Cross Site Request Forgery (CSRF) Protection Middleware
            // https://book.cakephp.org/4/en/security/csrf.html#cross-site-request-forgery-csrf-middleware
            // ->add(new CsrfProtectionMiddleware([
            //     'httponly' => true,
            // ]));


            $csrf = new CsrfProtectionMiddleware();
            $csrf->skipCheckCallback(function($request) {
                if (mb_strpos($request->getUri()->getPath(), '/api/') === 0) {
                    return true;
                }
            });

            $middlewareQueue->add($csrf);

        return $middlewareQueue;
    }

    public function getAuthenticationService(ServerRequestInterface $request): AuthenticationServiceInterface
    {
        $fields = [
            IdentifierInterface::CREDENTIAL_USERNAME => 'email',
            IdentifierInterface::CREDENTIAL_PASSWORD => 'password'
        ];

        //for API interface
        if (mb_strpos($request->getUri()->getPath(), '/api/') === 0) {
            $service = new AuthenticationService();
            $service->loadAuthenticator('Authentication.Form', [
                'fields' => $fields
            ]);
            $service->loadIdentifier('Authentication.JwtSubject');

            // https://book.cakephp.org/authentication/2/en/authenticators.html#jwt
            // Load the authenticators.
            $service->loadAuthenticator('Authentication.Jwt', [
                'secretKey' => file_get_contents(CONFIG . '/jwt.pem'),
                'algorithm' => 'RS256',
                'returnPayload' => false
            ]);

            $service->loadIdentifier('Authentication.Password', compact('fields'));

            return $service;
        }

        //for web ui interface
        $service = new AuthenticationService();
        // Define where users should be redirected to when they are not authenticated
        // $service->setConfig([
        //     'unauthenticatedRedirect' => Router::url([
        //             'prefix' => 'Admin',
        //             'plugin' => null,
        //             'controller' => 'Users',
        //             'action' => 'login',
        //     ]),
        //     'queryParam' => 'redirect',
        // ]);

        // Load the authenticators. Session should be first.
        $service->loadAuthenticator('Authentication.Session');
        $service->loadAuthenticator('Authentication.Form', [
            'fields' => $fields,
            'loginUrl' => Router::url([
                'prefix' => 'Admin',
                'plugin' => null,
                'controller' => 'Users',
                'action' => 'login',
            ])
        ]);

        // Load identifiers
        $service->loadIdentifier('Authentication.Password', compact('fields'));

        return $service;
    }

    public function getAuthorizationService(ServerRequestInterface $request): AuthorizationServiceInterface
    {
        $resolver = new OrmResolver();
        return new AuthorizationService($resolver);
    }

    /**
     * Register application container services.
     *
     * @param \Cake\Core\ContainerInterface $container The Container to update.
     * @return void
     * @link https://book.cakephp.org/4/en/development/dependency-injection.html#dependency-injection
     */
    public function services(ContainerInterface $container): void
    {
    }

    /**
     * Bootstrapping for CLI application.
     *
     * That is when running commands.
     *
     * @return void
     */
    protected function bootstrapCli(): void
    {
        $this->addOptionalPlugin('Cake/Repl');
        $this->addOptionalPlugin('Bake');

        $this->addPlugin('Migrations');

        // Load more plugins here
    }
}

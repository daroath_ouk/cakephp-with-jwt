<?php
declare(strict_types=1);

namespace App\Controller\Api;

use Cake\Event\EventInterface;
use Cake\Controller\Controller;

class AppController extends Controller
{

    protected $userId;
    protected $user;

    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Authentication.Authentication');
        $this->RequestHandler->renderAs($this, 'json');

        // Parser token
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $data = $result->getData();
            $this->user = $data;
            $this->userId = $data->id;
        }
    }

    public function beforeFilter(EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['login', 'add']);
    }
}
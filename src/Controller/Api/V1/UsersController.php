<?php
declare(strict_types=1);

namespace App\Controller\Api\V1;

use App\Controller\Api\AppController;
use Cake\View\JsonView;
use Cake\Event\EventInterface;
use Cake\Utility\Security;
use Firebase\JWT\JWT;

class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
    }

    public function viewClasses(): array
    {
        return [JsonView::class];
    }

    public function index()
    {
        // if (!$this->Auth->user('login_token')) {
        //     $data = [
        //         'error' => true,
        //         'message' => 'Login required.',
        //         'status' => 401
        //     ];
        //     $this->set(compact('data'));
        //     $this->viewBuilder()->setOption('serialize', ['data']);
        // } else {
            $users = $this->Users->find('all')->all();
            $this->set(compact('users'));
            $this->viewBuilder()->setOption('serialize', ['users']);
        // }
        
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
        $this->viewBuilder()->setOption('serialize', ['user']);
    }

    public function add()
    {
        $this->request->allowMethod(['post', 'put']);
        $user = $this->Users->newEntity($this->request->getData());
        if ($this->Users->save($user)) {
            $message = 'Saved';
        } else {
            $message = 'Error';
        }
        $this->set([
            'message' => $message,
            'user' => $user,
        ]);
        $this->viewBuilder()->setOption('serialize', ['user', 'message']);
    }

    public function login()
    {
        $result = $this->Authentication->getResult();
        if ($result->isValid()) {
            $privateKey = file_get_contents(CONFIG . '/jwt.key');  //access private key 
            $user = $result->getData();
            $payload = [
                'iss' => 'miles',
                'sub' => $user->id,
                'exp' => strtotime('+1 hour') //strtotime('+1 week'),    // given 1 week token expiry 
            ];
 
            $json = [
                'token' => JWT::encode($payload, $privateKey, 'RS256'),   //used RS256 algo
            ];

            $this->Users->patchEntity($user, ['login_token' => $json['token']]);
            $this->Users->save($user);
        } else {
            $this->response = $this->response->withStatus(401);
            $json = [];
        }

        $this->set(compact('json'));
        $this->viewBuilder()->setOption('serialize', 'json');
    }
}

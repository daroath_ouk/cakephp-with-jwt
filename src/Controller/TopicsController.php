<?php
declare(strict_types=1);

namespace App\Controller;
use Cake\View\JsonView;

class TopicsController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['index']);
    }

    public function viewClasses(): array
    {
        return [JsonView::class];
    }

    public function index()
    {
        $users = ['user' => 'daroath', 'password' => 'something'];
        $this->set('users', $users);
        $this->viewBuilder()->setOption('serialize', ['users']);
    }

    public function view($id = null)
    {
        $this->loadModel('Users');
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('user'));
        $this->viewBuilder()->setOption('serialize', ['user']);
    }
}

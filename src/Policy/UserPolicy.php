<?php
declare(strict_types=1);

namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;

class UserPolicy
{
    public function canCreate(IdentityInterface $user, User $resource)
    {
        return true;
    }

    public function canUpdate(IdentityInterface $user, User $resource)
    {
        return true;
    }

    public function canDelete(IdentityInterface $user, User $resource)
    {
        return true;
    }

    public function canView(IdentityInterface $user, User $resource)
    {
        return true;
    }
}

<?php

use Cake\Routing\Route\DashedRoute;
use Cake\Routing\RouteBuilder;
use Cake\Http\Middleware\CsrfProtectionMiddleware;

return static function (RouteBuilder $routes) {

    $routes->setRouteClass(DashedRoute::class);
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware());

    $routes->scope('/', function (RouteBuilder $builder) {

        $builder->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);
        $builder->connect('/pages/*', 'Pages::display');

        $builder->resources('Topics');
        $builder->connect('/topics', ['controller' => 'Topics', 'action' => 'index']);

        $builder->fallbacks(DashedRoute::class);
        $builder->fallbacks();
    });

    $routes->scope('/api/v1', ['prefix' => 'Api/V1'], function (RouteBuilder $builder) {
        $builder->setRouteClass(DashedRoute::class);
        $builder->setExtensions(['json']);
        $builder->resources('Users');
        // $builder->resources('Users', [
        //     'prefix' => 'Api',
        // ]);

        $builder->post('/users/login', 
            ['controller' => 'Users', 'action' => 'login']
        );

        // $builder->post('/users/add', 
        //     ['controller' => 'Users', 'action' => 'add', 'prefix' => 'api']
        // );

        // $builder->get('/users/{id}', 
        //     ['controller' => 'Users', 'action' => 'view', 'prefix' => 'api']
        // )->setPass(['id']);
        
        // $builder->get('/users',
        //     ['controller' => 'Users', 'action' => 'index', 'prefix' => 'api']
        // );
    });

    $routes->prefix('Admin', ['_namePrefix' => 'admin:'], function (RouteBuilder $routes) {
        $routes->connect('/', ['controller' => 'Users', 'action' => 'index']);
        $routes->fallbacks(DashedRoute::class);
    });

};
